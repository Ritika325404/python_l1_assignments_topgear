def convert(data):
    p=0
    if data<=8:
        for i in range(data):
            p+=2**i
        return(str(p)+".0.0.0")
    elif data>8 and data<=16:
        for i in range(data-8):
            p+=2**i
        return("255."+str(p)+".0.0")
    elif data>16 and data<=24:
        for i in range(data-16):
            p+=2**i
        return("255.255."+str(p)+".0")
    else:
        for i in range(data-24):
            p+=2**i
        return("255.255.255."+str(p))
